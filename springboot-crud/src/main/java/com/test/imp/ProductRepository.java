package com.test.imp;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.bean.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

}
